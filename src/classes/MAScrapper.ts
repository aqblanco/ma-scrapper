import { AlbumData, MetalArchivesAlbumReference } from "../types/album";
import { ArtistData, MetalArchivesArtistReference } from "../types/artist";

import request from 'request-promise-native';
import cheerio from 'cheerio';
import moment from 'moment';

export class MAScrapper {
	/**
	 * Get the list of releases from Metal Archives
	 * @param start 
	 * @param step 
	 * @param result 
	 */
	public getReleasesList(start: number, step: number, result: AlbumData[] = []): Promise<AlbumData[]> {
		return new Promise((resolve, reject) => {
			
			this.getReleasesData(start, step)
			.then((data) => {
				const last = data.nItems;
				if (data.data.length > 0) Array.prototype.push.apply(result, data.data);
				
				if (start > last) resolve(result);
				else resolve(this.getReleasesList(start + step, step, result));
			})
			.catch(e => {
				reject(e);
			});
		});
	}

	private getReleasesData(start: number, nItems: number): Promise<{nItems: number, data: AlbumData[]}> {
		return new Promise((resolve, reject) => {

			let aData: AlbumData[] = [];
			let result = {
				nItems: 0,
				data: aData,
			};

			let url = 'https://www.metal-archives.com/release/ajax-upcoming/json/1' +
			'?sEcho=1&iColumns=5&sColumns=&iDisplayStart=' + start + '&iDisplayLength=100' +
			'&mDataProp_0=0&mDataProp_1=1&mDataProp_2=2&mDataProp_3=3&mDataProp_4=4' + 
			'&iSortCol_0=4&sSortDir_0=asc&iSortingCols=1&bSortable_0=true&bSortable_1=true&bSortable_2=true&bSortable_3=true&bSortable_4=true&_=1573756863818';
			
			request({
				url: url,
				json: true
			})
			.then(data => {
				result.nItems = data.iTotalDisplayRecords;
				return this.parseReleasesData(data.aaData);
			})
			.then(data => {
				result.data = data;
				resolve(result);
			})
			.catch(e => {
				reject(e);
			});
		});
	}

	public getAlbumsMoreInfo(albumData: AlbumData[]): Promise<AlbumData[]> {
		return new Promise((resolve, reject) => {
			// Promisify the retrieving of the data for each album, generating an array
			let promiseArr: Promise<AlbumData>[] = albumData.map((e, i, a) => {
				return new Promise((resolve, reject) => {
					this.getSingleAlbumMoreInfo(e).then((info) => {
						a[i].label = info.label;
						resolve(a[i]);
					});
				})
			});

			// Resolve the resultant array when all promises finished
			Promise.all(promiseArr).then(res => {
				resolve(res);
			})
			.catch(e => {
				reject(e);
			});
		});
	}

	public getSingleAlbumMoreInfo(album: AlbumData): Promise<AlbumData> {
		return new Promise((resolve, reject) => {
			request(album.references.metalArchives.url).then(response => {
				const $ = cheerio.load(response);

				album.label = $('dl.float_right').find('dd').first().text();
				resolve(album);
			}).catch((err) => {
				reject(err);
			});
		});
	}

	public getArtistsMoreInfo(albumData: AlbumData[]): Promise<AlbumData[]> {
		return new Promise((resolve, reject) => {
			let promiseArr: Promise<AlbumData>[] = albumData.map((e, i, a) => {
				return new Promise((resolve, reject) => {
					this.getSingleArtistMoreInfo(e).then((info) => {
						a[i].artist.country = info.artist.country;
						resolve(a[i]);
					});
				})
			});

			// Resolve the resultant array when all promises finished
			Promise.all(promiseArr).then(res => {
				resolve(res);
			})
			.catch();
		});
	}

	public getSingleArtistMoreInfo(album: AlbumData): Promise<AlbumData> {
		const countryCodeRegex = /https:\/\/www\.metal-archives\.com\/lists\/([A-Z]{2})/
		return new Promise((resolve, reject) => {
			request(album.artist.references.metalArchives.url).then(response => {
				let $ = cheerio.load(response);

				const countryListURL = $('dl.float_left').find('dd').first().find('a').attr('href');
				const countryCodeMatch = countryListURL.match(countryCodeRegex);
				album.artist.country = countryCodeMatch[1];

				resolve(album);
			}).catch(err => {
				console.log(err);
			});
		});
	}

	public cleanData(data: AlbumData[]): {removed: number, data: AlbumData[]} {
		let removed = 0;

		for (let i = data.length - 1; i >= 0; i -= 1) {
			for (let j = data.length - 1; j >= 0; j -= 1) {
				// Don't check against itself
				if (data[i].references.metalArchives.id != data[j].references.metalArchives.id) {
					const sameTitle = data[i].name == data[j].name;
					const sameArtist = data[i].artist.references.metalArchives.id == data[j].artist.references.metalArchives.id;
					const sameType = data[i].type == data[j].type;
					const similarType = (
						(data[i].type == 'Compilation' && data[j].type == 'Boxed set') ||
						(data[i].type == 'Boxed set' && data[j].type == 'Compilation')
					);
					const excludedType = data[i].type == 'Split';

					if ((sameTitle && sameArtist && (sameType || similarType )) || excludedType) {
						data.splice(i, 1);
						removed++;
					}
				}
			}
		}

		let ret = {
			removed: removed,
			data: data
		};
		
		return ret;
	}

	private parseReleasesData(data: any[]): Promise<AlbumData[]> {
		return new Promise((resolve, reject) => {
			const regexpAlbumUrl = /https:\/\/www\.metal-archives\.com\/albums\/.+\/.+\/(\d+)/;
			const regexpArtistUrl = /https:\/\/www\.metal-archives\.com\/bands\/.+\/(\d+)/;
			let result: AlbumData[] = [];

			data.forEach(e => {
				// Artist data
				let $ = cheerio.load(e[0]);
				const artistUrl = $('a').attr('href');
				const artistUrlMatch = artistUrl.match(regexpArtistUrl);
				const artistName = $('a').text();
				const artistID = artistUrlMatch[1];
				const maArtistRef: MetalArchivesArtistReference = {
					id: artistID,
					url: artistUrl,
				}
				const artist: ArtistData = {
					name: artistName,
					references: {
						metalArchives: maArtistRef
					},
				}			

				// Album data
				$ = cheerio.load(e[1]);
				const albumUrl = $('a').attr('href');
				const albumUrlMatch = albumUrl.match(regexpAlbumUrl);
				const albumName = $('a').text();
				const albumID = albumUrlMatch[1];
				
				const maAlbumRef: MetalArchivesAlbumReference = {
					id: albumID,
					url: albumUrl,
				}
					
				let albumData: AlbumData = {
					artist: artist,
					name: albumName,
					type: e[2],
					genre: e[3],
					release: moment(e[4], 'MMMM Do, YYYY').toDate(),
					priority: 0,
					references: {
						metalArchives: maAlbumRef
					},
				}
				result.push(albumData);
			});

			resolve(result);
		});
	}
}