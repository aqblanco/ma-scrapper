export { MAScrapper } from './classes/MAScrapper';

// Typings
export * from './types/album';
export * from './types/artist';