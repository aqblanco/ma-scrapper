export interface MetalArchivesArtistReference {
	id: string,
	url: string,
}

export interface ArtistData {
	name: string,
	country?: string,
    bandcampAcc?: string,
    facebookAcc?: string,
    soundcloudAcc?: string,
	twitterAcc?: string,
	references?:  {
		metalArchives?: MetalArchivesArtistReference,
	}
}