import { ArtistData } from "./artist";

export interface MetalArchivesAlbumReference {
	id: string,
	url: string,
}

export interface AlbumData {
	name: string,
	artist: ArtistData,
	genre?: string,
    priority?: number,
    release?: Date,
	type?: string,
	label?: string,
	cover?: string,
	references?:  {
		metalArchives?: MetalArchivesAlbumReference,
	}
}